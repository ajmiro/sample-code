"use strict";
/**
 * This lambda function fetches hours billed this month and stores them in a spreadsheet that then powers a datastudio report. The function should be invoked via a CloudWatch cron.
 * @module Lambdas/getIssuesOnPending
 * @requires aws-sdk
 * @requires Core/Accelo
 * @requires Core/Contants
 * @requires Core/Logger
 * @requires Core/SlaDate
 * @requires fs
 * @requires moment-timezone
 */
const ACCELO = require('d6-accelo/accelo')
const CONSTANTS = require('./constants.js')
const FS = require('fs')
const HELPERS = require('d6-accelo/helpers')
const LOGGER = require('d6-accelo/logger')
const MOMENT = require('moment-timezone')
const SHAREDCONSTANTS = require('d6-accelo/constants')
const SLADATE = require('d6-accelo/sla-date')
const AWS = require('aws-sdk')
const BIG_QUERY = require('@google-cloud/bigquery')

MOMENT.tz.setDefault("UTC")

let accelo = null
let bq
let handlerCallback
let logfile = null
let s3, slaDate

/**
 * Process complete. Wraps the response with correct headers and statusCodes for API Gateway.
 * @param {Object} error
 * @param {Object} result
 * @param {Function} callback
 * @return {void}
 */
function done(error, result) {
	if(error)
	{
		LOGGER.log(error.message, 'error', error, logfile)
		handlerCallback(null, {
			statusCode: 500,
			body: JSON.stringify(error)
		})
	}
	else
	{
		handlerCallback(null, {
			statusCode: 200,
			body: JSON.stringify(result)
		})
	}
}
/**
 * Returns holidays stored s3 bucket
 * @return {array} list of sla holidays
 */
function loadHolidays(){
    return new Promise((resolve, reject)  => {
        if( process.env.NODE_ENV === 'production') {
            s3.getObject({
                Bucket: SHAREDCONSTANTS.SLA_BUCKET,
                Key: SHAREDCONSTANTS.SLA_HOLIDAYS + '.json'
            }, (error, data) => {
                if( null ===  error )
                {
                    resolve(data.Body.toString())
                }
                else
                {
                    reject(error)
                }
            })
        }
        else
        {
            resolve(JSON.parse(FS.readFileSync('./data/getSLAHolidays.json', 'utf-8')).data)
        }
    })
}
/**
 * load resolved issue
 * @return {promise}
 */
function loadIssue( issue_id ) {
	LOGGER.log(CONSTANTS.MESSAGE_LOADING_ISSUE, LOGGER.LEVEL_INFO, issue_id, logfile)

    return new Promise((resolve,reject) => {
        accelo.getObject("issues/" + issue_id, {_fields:['_ALL', 'class()', 'contact()', 'issue_type()']}, (error, item) => {
                if(error)
                {
                    reject({
                        message: CONSTANTS.ERROR_LOADING_ISSUE,
                        error: error
                    })
                }
                else
                {
                    if( item && (item.standing === CONSTANTS.STANDING_RESOLVED || item.standing === CONSTANTS.STANDING_CLOSED) )
                    {

                        let temp = {
                                id: item.id,
                                issue_id: item.id,
                                against_id: item.company,
                                against_type: item.against_type,
                                class: (item.class !== null) ? item.class.title : item.class,
                                contact: (item.contact) ? item.contact.firstname + ' ' + item.contact.surname : null,
                                contract_id: item.contract,
                                date_due: item.date_due,
                                date_opened: item.date_opened ? item.date_opened : new MOMENT().tz('Pacific/Auckland').unix(),
                                date_resolved: item.date_resolved,
                                date_submitted: item.date_submitted,
                                date_closed_nz: item.date_closed ? MOMENT.unix(item.date_closed).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_due_nz: item.date_due ? MOMENT.unix(item.date_due).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_opened_nz: item.date_opened ? MOMENT.unix(item.date_opened).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : new MOMENT().tz('Pacific/Auckland').format('YYYY-MM-DD HH:mm:ss') ,
                                date_resolved_nz: item.date_resolved ? MOMENT.unix(item.date_resolved).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_submitted_nz: item.date_submitted ? MOMENT.unix(item.date_submitted).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                issue_priority: item.issue_priority,
                                issue_type: (item.issue_type !== null ) ? item.issue_type.title : null,
                                standing: item.standing,
                                issue_status: item.status,
                                title: item.title,
                                timestamp: MOMENT.utc().format(),
                                date_received_nz: MOMENT.unix(item.date_submitted).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss'),
                                date_responded_nz: MOMENT.unix(item.date_opened).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss'),
                                object_type: 'tickets',
                                object_id: 't' + item.id
                            }
                        resolve(temp)
                    }
                    else
                    {
                        resolve(item)
                    }

                }
         })
    })
}

/**
 * Insert to big query
 * @param  {Array} data
 * @return {Promise}
*/
function insertToBigQuery( ds, table, data ){
     return new Promise((resolve, reject) => {
         bq.dataset(ds)
               .table(table)
                 .insert(data, {}, (error, result) => {
                     if( null === error )
 					{
 						resolve(result)
 					}
                     else
 					{
                         reject({
 							message: CONSTANTS.ERROR_BIQ_QUERY_INSERT,
 							error: error
 						})
                     }
                 })
     })
 }
/**
 * Main handler function
 * @param  {object}   event
 * @param  {object}   context
 * @param  {Function} callback
 * @return {void}
 */
exports.handler = (event, context, callback) => {
	try{
		// Load the config from either the event (in the case of the tests), or from .env(.dev) file for development or from the process.env.CREDENTIALS for production
		const CONFIG = (event.credentials ? event.credentials : (process.env.NODE_ENV == "production" ? JSON.parse(process.env.CREDENTIALS) : JSON.parse(FS.readFileSync('.dev'))))
		const ISSUE_ID = JSON.parse(event.body).id

        accelo = (event.accelo ? event.accelo : new ACCELO(CONFIG.accelo))
        s3 = (event.s3 ? event.s3 : new AWS.S3({apiVersion: '2006-03-01'}))
        handlerCallback = callback
        logfile = event.logfile ? event.logfile : null
		bq = (event.bq ?  event.bq : new BIG_QUERY({credentials: CONFIG.google}))

		// Authenticate
        var secret = (event.secret ? event.secret : process.env.SECRET)
        var authenticateToken = event.authenticateToken ? event.authenticateToken : accelo.authenticateToken;

        LOGGER.log(CONSTANTS.MESSAGE_AUTHENTICATING, LOGGER.INFO, {token: event.headers["X-Hub-Signature"], body: event.body}, logfile)

        if(!authenticateToken(secret, event.headers["X-Hub-Signature"], event.body))
        {
            return done({message: CONSTANTS.ERROR_AUTH, signature: event.headers["X-Hub-Signature"], body: event.body})
        }


		// Connect to API
		accelo.connect((error, result) => {

			// Connection error
			if(error) return done({
				message: CONSTANTS.ERROR_ACCELO_CONNECT,
				error: error
			})

			LOGGER.log("Connected", LOGGER.LEVEL_INFO, null, logfile)


			let issuePromise = loadIssue(ISSUE_ID),
				holidaysPromise = loadHolidays(),
                extensionFieldValues = new Promise((resolve, reject) => {
                    accelo.list('issues/'+ ISSUE_ID +'/extensions/values', {_fields: ['_ALL'], _filters: null}, (error, values)=>{
                        try{FS.writeFileSync('./debug/'+context.functionName+'_getExtensionsValues.json', JSON.stringify(values, "\n", 4))}catch(e){}
                        if(error) reject({message: CONSTANTS.ERROR_GETTING_EXTENSION_FIELD_VALUES, error: error})
                        else resolve(values)
                    })
                }),
                profileFieldValues = new Promise((resolve, reject) => {
                    accelo.list('issues/'+ ISSUE_ID +'/profiles/values', {_fields: ['_ALL'], _filters: null}, (error, values)=>{
                        try{FS.writeFileSync('./debug/'+context.functionName+'_getProfileValues.json', JSON.stringify(values, "\n", 4))}catch(e){}
                        if(error) reject({message: CONSTANTS.ERROR_GETTING_EXTENSION_FIELD_VALUES, error: error})
                        else resolve(values)
                    })
                })

			Promise.all([issuePromise, holidaysPromise, extensionFieldValues, profileFieldValues])
                .then( result => {
                    let issue = result[0],
						holidays = result[1],
						promises = []

                    if( issue && (issue.standing === CONSTANTS.STANDING_RESOLVED || item.standing === CONSTANTS.STANDING_CLOSED) )
                    {
                        if( process.env.NODE_ENV === 'production') {
                            holidays = JSON.parse( result[1] )
                            slaDate = new SLADATE(holidays.data)
                        } else {
                            slaDate = new SLADATE(holidays)
                        }

						console.log( issue )

                        // @TODO: Remove line 248 - 270 and add to a separate lambda script
                        let dateResolved = slaDate.setTimeToOfficeHours(new MOMENT(issue.date_resolved_nz))
                        let dateOpened = slaDate.setTimeToOfficeHours(new MOMENT(issue.date_responded_nz))

                        issue.resolutiontimeinseconds = slaDate.getTimeDifferenceInSeconds(dateOpened, dateResolved, 0)

                        if( null !== issue.date_due )
                        {
                            let dateDue = slaDate.setTimeToOfficeHours(new MOMENT(issue.date_due_nz))

                            if( issue.date_resolved > issue.date_due )
                            {
                                issue.resolutiontimestatus = SHAREDCONSTANTS.SLA_BROKEN
                                issue.resolutiontimeselapseinseconds = slaDate.getTimeDifferenceInSeconds(dateDue, dateResolved, 0) * -1
                            }
                            else
                            {
                                issue.resolutiontimestatus = SHAREDCONSTANTS.SLA_MET
                                issue.resolutiontimeselapseinseconds = slaDate.getTimeDifferenceInSeconds(dateResolved, dateDue, 0)

                            }
                            issue.timediffbetweenresolveddueinseconds = issue.resolutiontimeselapseinseconds
                        }

                        let slaResponded = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SECONDS_TO_RESPOND)),
                            slaStatus = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SLAS_TATUS)),
                            customerRating = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_CUSTOMER_RATING)),
                            customerFeedback = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_CUSTOMER_FEEDBACK)),
                            slaRespondOld = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SLA_RESPONDED)),
                            billingOld = result[3].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ADHOC_AFTERHOURS)),
                            locationOld = result[3].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ONSITE_REMOTE)),
                            teamOld = result[3].filter( val =>  (val.field_id == CONSTANTS.PROFILE_DEV))

                            issue.slaresponded = ( slaResponded.length > 0 ) ? slaResponded[0].value : 0
                            issue.slastatus = ( slaStatus.length > 0 ) ? slaStatus[0].value : null
                            issue.customerrating = ( customerRating.length > 0 ) ? customerRating[0].value : 'No response'
                            issue.customerfeedback = ( customerFeedback.length > 0 ) ? customerFeedback[0].value : 'No feedback'

                        // @TODO: Add resolution status and time between resolved and due date
                        /* let resolutionTimeStatus = result2[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_RESOLUTION_TIME_STATUS && issue.issue_id === val.link_id )),
                            timeBetweenResolvedDue = result2[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_TIME_BETWEEN_RESOLVED_DUE && issue.issue_id === val.link_id ))

                        temp.resolutiontimestatus = ( resolutionTimeStatus.length > 0 ) ? resolutionTimeStatus[0].value : null
                        temp.resolutiontimeselapseinseconds = temp.timediffbetweenresolveddueinseconds = ( timeBetweenResolvedDue.length > 0 ) ? timeBetweenResolvedDue[0].value : null
                        */

                        // Tickets created after 12pm 16 Jan 2019 when trigger from Peter's end is removed
                        if(issue.issue_id < 39349) {
                            issue.slaresponded = (slaRespondOld.length > 0 ) ? slaRespondOld[0].value : 0
                        }

                        // issue.adhocorafterhours = (billingOld.length > 0) ? billingOld[0].value : result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ADHOC_AFTERHOURS))[0].value
                        // issue.onsiteorremote = (locationOld.length > 0) ? locationOld[0].value :  result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ONSITE_REMOTE))[0].value
                        // issue.dev = (teamOld.length > 0) ? teamOld[0].value : result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_DEVELOPMENT))[0].value

                        if(issue.issue_id > 39394)
                        {
                            issue.adhocorafterhours = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ADHOC_AFTERHOURS))[0].value
                        	issue.onsiteorremote = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ONSITE_REMOTE))[0].value
                        	issue.dev = result[2].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_DEVELOPMENT))[0].value

                        }
                        else
                        {
                            issue.adhocorafterhours = billingOld[0].value
                        	issue.onsiteorremote = locationOld[0].value
                        	issue.dev = teamOld[0].value
                        }

                        delete issue.id
                        delete issue.date_received_nz
                        delete issue.date_responded_nz

                        Promise.all([issue])
                            .then( result2 => {
                                if( process.env.NODE_ENV === 'production')
                                {
                                    insertToBigQuery( CONSTANTS.BIG_QUERY_DATASET, CONSTANTS.BIG_QUERY_TABLE, result2)
                                        .then( results3 => {
                                            return done(null, results3)
                                        })
                                        .catch( error => {
                                            return done( error )
                                        })
                                }
                                else
                                {
                                    FS.writeFileSync('./debug/getIssueOnResolved.json', JSON.stringify(result2, "\n", 4))
                                    return done( error )
                                }
                            })
                            .catch( error => {
								console.log(error)
                                return done(null, error)
                            })


                    }
                    else
                    {
                        return done(null, [])
                    }
                })
				.catch( error => {
					console.log(error)
					return done(null, error)
				})

		})

	} catch (e) {
		done(e)
	}

}
