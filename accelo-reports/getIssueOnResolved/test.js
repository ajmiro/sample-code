'use strict'
// Constants
const EXPECT = require('chai').expect
const LAMBDA = require('./index.js')
const FS = require('fs')
const PATH = require('path')
const CONSTANTS = Object.assign(require('./constants.js'), require('d6-accelo/constants'))
const CONTEXT = {functionName: PATH.basename(__dirname)}
const LOG_FILE = './logs/' + CONTEXT.functionName + ".log"

process.env.NODE_ENV = 'test'
process.env.LOG_PATH = './logs/' + CONTEXT.functionName + ".log";


// The tests
describe("Lambda: " + CONTEXT.functionName, function() {
	let authenticateToken = () => true
	let event = {
		"resource": "/progressions/issues/get-on-resolved",
		"path": "/progressions/issues/get-on-resolved",
		"httpMethod": "POST",
		"headers": {
			"Content-Type": "application/json",
			"Host": "muag43srvh.execute-api.us-east-1.amazonaws.com",
			"X-Accelo-Event": "create_issue",
			"X-Amzn-Trace-Id": "Root=1-5b3ee6aa-4537e0f060002bfc00dd135c",
			"X-Forwarded-For": "54.212.228.134",
			"X-Forwarded-Port": "443",
			"X-Forwarded-Proto": "https"
		},
		"queryStringParameters": null,
		"pathParameters": null,
		"stageVariables": null,
		"requestContext": {
			"resourceId": "zgruqp",
			"resourcePath": "/progressions/issues/get-on-resolved",
			"httpMethod": "POST",
			"extendedRequestId": "Jlj6kFXkIAMFU9Q=",
			"requestTime": "06/Jul/2018:03:48:58 +0000",
			"path": "/dev/progressions/issues/get-on-resolved",
			"accountId": "605367074299",
			"protocol": "HTTP/1.1",
			"stage": "dev",
			"requestTimeEpoch": 1530848938074,
			"requestId": "83177c7f-80cf-11e8-b030-1d0284cbb544",
			"identity": {
				"cognitoIdentityPoolId": null,
				"accountId": null,
				"cognitoIdentityId": null,
				"caller": null,
				"sourceIp": "54.212.228.134",
				"accessKey": null,
				"cognitoAuthenticationType": null,
				"cognitoAuthenticationProvider": null,
				"userArn": null,
				"userAgent": null,
				"user": null
			},
			"apiId": "muag43srvh"
		},
		"body": "{\"resource_url\":\"https://dynamo6.api.accelo.com/api/v0/issues/39373.json\",\"id\":\"39373\"}",
		"isBase64Encoded": false,
		// Extra TEST parameters
		// accelo: Object.assign({}, acceloMockService),
		authenticateToken: authenticateToken,
		// credentials: JSON.parse(FS.readFileSync('.dev')),
		logfile: './logs/' + CONTEXT.functionName + ".log",
		secret: ''
	}

	it('Should load issue successfully (mock)', function(done){
		this.timeout(60000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)
				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
})
