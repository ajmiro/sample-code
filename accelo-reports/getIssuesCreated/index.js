"use strict";
/**
 * This lambda function fetches hours billed this month and stores them in a spreadsheet that then powers a datastudio report. The function should be invoked via a CloudWatch cron.
 * @module Lambdas/getIssuesOnPending
 * @requires aws-sdk
 * @requires Core/Accelo
 * @requires Core/Contants
 * @requires Core/Logger
 * @requires Core/SlaDate
 * @requires fs
 * @requires moment-timezone
 */
const ACCELO = require('d6-accelo/accelo')
const AWS = require('aws-sdk')
const CONSTANTS = require('./constants.js')
const FS = require('fs')
const HELPERS = require('d6-accelo/helpers')
const LOGGER = require('d6-accelo/logger')
const MOMENT = require('moment-timezone')
const SHAREDCONSTANTS = require('d6-accelo/constants')
const SLADATE = require('d6-accelo/sla-date')
const BIG_QUERY = require('@google-cloud/bigquery')

MOMENT.tz.setDefault("UTC")

let accelo = null
let bq
let handlerCallback = null
let logfile = null
let s3, slaDate

/**
 * Process complete. Wraps the response with correct headers and statusCodes for API Gateway.
 * @param {Object} error
 * @param {Object} result
 * @param {Function} callback
 * @return {void}
 */
function done(error, result) {
	if(error)
	{
		LOGGER.log(error.message, 'error', error, logfile)
		handlerCallback(null, {
			statusCode: 500,
			body: JSON.stringify(error)
		})
	}
	else
	{
		handlerCallback(null, {
			statusCode: 200,
			body: JSON.stringify(result)
		})
	}
}
/**
 * Returns holidays stored s3 bucket
 * @return {array} list of sla holidays
 */
function loadHolidays(){
    return new Promise((resolve, reject)  => {
        if( process.env.NODE_ENV === 'production') {
            s3.getObject({
                Bucket: SHAREDCONSTANTS.SLA_BUCKET,
                Key: SHAREDCONSTANTS.SLA_HOLIDAYS + '.json'
            }, (error, data) => {
                if( null ===  error )
                {
                    resolve(data.Body.toString())
                }
                else
                {
                    reject(error)
                }
            })
        }
        else
        {
            resolve(JSON.parse(FS.readFileSync('./data/getSLAHolidays.json', 'utf-8')).data)
        }
    })
}
/**
 * load pending issues from accelo
 * @return {promise}
 */
function loadIssues( slaDate, filters ) {
	return new Promise((resolve, reject) => {
		accelo.list('issues', {
				_fields: ['_ALL', 'class()', 'contact()', 'issue_type()'],
				_filters: filters
		 	}, (error, result ) => {

				if(null === error)
				{
					let collated = []
					result.forEach((item) => {
						let temp = {
								id: item.id,
                                issue_id: item.id,
                                against_id: item.company,
                                against_type: item.against_type,
                                class: (item.class !== null) ? item.class.title : item.class,
                                contact: (item.contact) ? item.contact.firstname + ' ' + item.contact.surname : null,
                                contract_id: item.contract,
                                date_due: item.date_due,
                                date_opened: item.date_opened ? item.date_opened : new MOMENT().tz('Pacific/Auckland').unix(),
                                date_resolved: item.date_resolved,
                                date_submitted: item.date_submitted,
                                date_closed_nz: item.date_closed ? MOMENT.unix(item.date_closed).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_due_nz: item.date_due ? MOMENT.unix(item.date_due).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_opened_nz: item.date_opened ? MOMENT.unix(item.date_opened).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : new MOMENT().tz('Pacific/Auckland').format('YYYY-MM-DD HH:mm:ss') ,
                                date_resolved_nz: item.date_resolved ? MOMENT.unix(item.date_resolved).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                date_submitted_nz: item.date_submitted ? MOMENT.unix(item.date_submitted).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : null,
                                issue_priority: item.issue_priority,
                                issue_type: (item.issue_type !== null ) ? item.issue_type.title : null,
                                standing: item.standing,
                                issue_status: item.status,
                                title: item.title,
                                timestamp: MOMENT.utc().format(),
                                object_type: 'tickets',
                                object_id: 't' + item.id
						}

						collated.push(temp)
					})

					resolve(collated)
				}
				else
				{

					reject({
						message: CONSTANTS.ERROR_GETTING_ISSUES,
						error: error
					})
				}
		 })
	})
}

/**
 * Insert to big query
 * @param  {Array} data
 * @return {Promise}
*/
function insertToBigQuery( ds, table, data ){
	console.log(ds, table)
     return new Promise((resolve, reject) => {
         bq.dataset(ds)
               .table(table)
                 .insert(data, {}, (error, result) => {
                     if( null === error )
 					{
 						resolve(result)
 					}
                     else
 					{
                         reject({
 							message: CONSTANTS.ERROR_BIQ_QUERY_INSERT,
 							error: error
 						})
                     }
                 })
     })
 }
/**
 * Main handler function
 * @param  {object}   event
 * @param  {object}   context
 * @param  {Function} callback
 * @return {void}
 */
exports.handler = (event, context, callback) => {
	try{
		// Load the config from either the event (in the case of the tests), or from .env(.dev) file for development or from the process.env.CREDENTIALS for production
		const CONFIG = (event.credentials ? event.credentials : (process.env.NODE_ENV == "production" ? JSON.parse(process.env.CREDENTIALS) : JSON.parse(FS.readFileSync('.dev'))))

		accelo = (event.accelo ? event.accelo : new ACCELO(CONFIG.accelo))
		bq = (event.bq ?  event.bq : new BIG_QUERY({credentials: CONFIG.google}))
        s3 = (event.s3 ? event.s3 : new AWS.S3({apiVersion: '2006-03-01'}))
        handlerCallback = callback
        logfile = event.logfile ? event.logfile : null

		// Connect to API
		accelo.connect((error, result) => {

			// Connection error
			if(error) return done({
				message: CONSTANTS.ERROR_ACCELO_CONNECT,
				error: error
			})

			let holidaysPromise = loadHolidays()
			holidaysPromise
				.then( result => {
					let holidays = result

                    if( process.env.NODE_ENV === 'production')
					{
                        holidays = JSON.parse( result )
                        slaDate = new SLADATE(holidays.data)
                    }
					else
					{
                        slaDate = new SLADATE(holidays)
                    }

					let dateAfter = new MOMENT().startOf('day').subtract(1, 'day').unix() - 1,
						dateBefore = new MOMENT().endOf('day').subtract(1, 'day') + 1,
						filters = ['date_opened_after(' + dateAfter + ')']

					// On test, pass date_opened_after
					if( event.dateAfter && event.dateBefore ) {
						console.log( event.dateAfter, event.dateBefore )
						dateAfter = new MOMENT("2019-01-17").startOf('day').subtract(1, 'day').unix() - 1 // 15 - 17
						dateBefore = new MOMENT("2019-01-18").endOf('day').subtract(1, 'day').unix() - 1
						filters = ['_AND(date_opened_after(' + dateAfter + '), date_opened_before('+ dateBefore +'))']
					}

					console.log( filters )
					// On test, pass array of issue ids
					if( event.issueId ) {
						filters = ['id('+ event.issueId.toString() +')']
					}

					let issues = loadIssues( slaDate, filters )

					issues.then( issuesCreated => {
						console.log(issuesCreated.length)
						if( issuesCreated.length > 0 ) {
							let issuesIdList = issuesCreated.reduce((accumulator, value) => {
												return ( (accumulator.id) ? accumulator.id.toString() + "," + value.id.toString() : accumulator + "," + value.id.toString() )
											}),
								valuesExtensionFieldValues = new Promise((resolve, reject) => {
									accelo.list('issues/extensions/values', {_fields: ['_ALL'], _filters: ['_AND(link_id('+ issuesIdList +'),link_type(issue))', 'order_by_desc(id)']}, (error, values)=>{
										try{FS.writeFileSync('./debug/'+context.functionName+'_getExtensionsValues.json', JSON.stringify(values, "\n", 4))}catch(e){}
										if(error) reject({message: CONSTANTS.ERROR_GETTING_EXTENSION_FIELD_VALUES, error: error})
										else resolve(values)
									})
								}),
								valuesProfileFieldValues = new Promise((resolve, reject) => {
									accelo.list('issues/profiles/values', {_fields: ['_ALL'], _filters: ['_AND(link_id('+ issuesIdList +'),link_type(issue))', 'order_by_desc(id)']}, (error, values)=>{
										try{FS.writeFileSync('./debug/'+context.functionName+'_getProfileValues.json', JSON.stringify(values, "\n", 4))}catch(e){}
										if(error) reject({message: CONSTANTS.ERROR_GETTING_EXTENSION_FIELD_VALUES, error: error})
										else resolve(values)
									})
								})

							Promise.all([valuesExtensionFieldValues, valuesProfileFieldValues])
								.then( result2 => {

									let promises = []

									issuesCreated.forEach( issue => {
										let temp = issue,
											slaResponded = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SECONDS_TO_RESPOND && issue.issue_id === val.link_id)),
											slaStatus = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SLAS_TATUS && issue.issue_id === val.link_id)),
											customerRating = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_CUSTOMER_RATING && issue.issue_id === val.link_id)),
											customerFeedback = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_CUSTOMER_FEEDBACK && issue.issue_id === val.link_id)),
											slaRespondOld = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_SLA_RESPONDED && issue.issue_id === val.link_id))

										temp.slaresponded = ( slaResponded.length > 0 ) ? slaResponded[0].value : 0
										temp.slastatus = ( slaStatus.length > 0 ) ? slaStatus[0].value : null
										temp.customerrating = ( customerRating.length > 0 ) ? customerRating[0].value : 'No response'
										temp.customerfeedback = ( customerFeedback.length > 0 ) ? customerFeedback[0].value : 'No feedback'

										// Tickets created after 12pm 16 Jan 2019 when trigger from Peter's end is removed
										if(issue.issue_id < 39349) {
											temp.slaresponded = (slaRespondOld.length > 0 ) ? slaRespondOld[0].value : 0
										}

										temp.adhocorafterhours = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ADHOC_AFTERHOURS && issue.issue_id === val.link_id))[0].value
										temp.onsiteorremote = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ONSITE_REMOTE && issue.issue_id === val.link_id))[0].value
										temp.dev = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_DEV && issue.issue_id === val.link_id))[0].value

										// Get the old profile fields; ticket created before 16 Jan 2019
										// if( issue.issue_id < 39349 ) {
										// 	temp.adhocorafterhours = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ADHOC_AFTERHOURS && issue.issue_id === val.link_id))[0].value
										// 	temp.onsiteorremote = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_ONSITE_REMOTE && issue.issue_id === val.link_id))[0].value
										// 	temp.dev = result2[1].filter( val =>  (val.field_id == CONSTANTS.PROFILE_DEV && issue.issue_id === val.link_id))[0].value
										// }
										// else
										// {
										// 	temp.adhocorafterhours = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ADHOC_AFTERHOURS && issue.issue_id === val.link_id))[0].value
										// 	temp.onsiteorremote = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_ONSITE_REMOTE && issue.issue_id === val.link_id))[0].value
										// 	temp.dev = result2[0].filter( val =>  (val.field_id == CONSTANTS.EXTENSION_DEVELOPMENT && issue.issue_id === val.link_id))[0].value
										// }

										delete temp.id

										promises.push(temp)
									})

									Promise.all(promises)
										.then( finalResult => {

											if( process.env.NODE_ENV === 'production')
											{
												insertToBigQuery( CONSTANTS.BIG_QUERY_DATASET, CONSTANTS.BIG_QUERY_TABLE, finalResult)
													.then( results3 => {
														return done(null, results3)
													})
													.catch( error => {
														return done( error )
													})
											}
											else
											{
												FS.writeFileSync('./debug/createdIssues.json', JSON.stringify(finalResult, "\n", 4))
												return done(null, finalResult )

											}
										})
										.catch( error => {
											return done(error)
										})

								})
								.catch( error => {
									console.log( error )
									return done(error)
								})
						}
						else
						{
							return done(null, {})
						}

					})
					.catch( error => {
						return done( error )
					})

				}).catch( error => {
					return done( error )
				})
		})
	} catch (e) {
		done(e)
	}

}
