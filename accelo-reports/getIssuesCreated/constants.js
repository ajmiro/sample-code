module.exports = {
	BIG_QUERY_DATASET: "d6accelo",
	BIG_QUERY_TABLE: "issues",
	ISSUES_UPDATED_TODAY: 'issues-updated-today',
	ISSUES_CUSTOM_FIELDS_TODAY: 'issues-custom-fields-today',
	S3_BUCKET: "accelo-updated-issues",
	ERROR_GETTING_EXTENSION_FIELD_VALUES: "Error getting extension value of issues",
	EXTENSION_SECONDS_TO_RESPOND: 108,
	EXTENSION_SLA_RESPONDED: 68,
	EXTENSION_SLAS_TATUS: 65,
	EXTENSION_CUSTOMER_RATING: 99,
	EXTENSION_CUSTOMER_FEEDBACK: 100,
	EXTENSION_DEVELOPMENT: 129,
	EXTENSION_ADHOC_AFTERHOURS: 130,
	EXTENSION_ONSITE_REMOTE: 131,
	PROFILE_ADHOC_AFTERHOURS: 11,
	PROFILE_ONSITE_REMOTE: 13,
	PROFILE_DEV: 18
}
