'use strict'
// Constants
const EXPECT = require('chai').expect
const LAMBDA = require('./index.js')
const FS = require('fs')
const PATH = require('path')
const CONSTANTS = Object.assign(require('./constants.js'), require('d6-accelo/constants'))
const CONTEXT = {functionName: PATH.basename(__dirname)}
const LOG_FILE = './logs/' + CONTEXT.functionName + ".log"

process.env.NODE_ENV = 'test'
process.env.LOG_PATH = './logs/' + CONTEXT.functionName + ".log";


// The tests
describe("Lambda: " + CONTEXT.functionName, function() {
	let acceloMockService = {
		connect: (cb) => cb(),
		list: (endpoint, params, callback) => callback(null, JSON.parse(FS.readFileSync('./data/' + CONSTANTS.ISSUES_UPDATED_TODAY +'.json', 'utf-8')).data )
	}
	let S3MockService = {
		putObject: (params, callback) => {
			FS.writeFileSync('./debug/s3/' + params.Key, params.Body)
			callback(null, {
				data: {
					ETag: "\"6805f2cfc46c0f04559748bb039d69ae\"",
					VersionId: "psM2sYY4.o1501dSx8wMvnkOzSBB.V4a"
				}
			})
		}
	}

	// Uncomment below to get resolved tickets in accelo
	// it('Should load issues updated today (live) and save locally (mock)', function(done){
	// 	this.timeout(200000)
	// 	LAMBDA.handler({
	// 		logfile: LOG_FILE,
	// 		dateAfter: "2019-01-14",
	// 		dateBefore: "2019-01-18"
	// 	}, CONTEXT, (error, result) => {
	//
	// 		EXPECT(result.statusCode).to.equal(200)
	// 		done()
	//
	// 	})
	// })


})
