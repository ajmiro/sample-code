"use strict";
/**
 * This lambda function set due date of newly opened ticket
 * @module Lambdas/setIssueDueDateCreated
 * @requires aws-sdk
 * @requires Core/Accelo
 * @requires Core/Logger
 * @requires Core/SlaDate
 * @requires crypto
 * @requires fs
 * @requires moment-timezone
 * @example
 * Example Invocation:
 * {
 *    "resource": "/setissueduedatecreated",
 *	    "path": "/setissueduedatecreated",
 *	    "httpMethod": "POST",
 *	    "headers": {
 *	        "Content-Type": "application/json",
 *	        "Host": "muag43srvh.execute-api.us-east-1.amazonaws.com",
 *	        "X-Accelo-Event": "progress_issue",
 *	        "X-Amzn-Trace-Id": "Root=1-5b45c2e7-12836bd6176ac0cceb7555c8",
 *	        "X-Forwarded-For": "34.214.71.183",
 *	        "X-Forwarded-Port": "443",
 *	        "X-Forwarded-Proto": "https"
 *	    },
 *	    "queryStringParameters": null,
 *	    "pathParameters": null,
 *	    "stageVariables": null,
 *	    "requestContext": {
 *	        "resourceId": "ztcevc",
 *	        "resourcePath": "/setissueduedatecreated",
 *	        "httpMethod": "POST",
 *	        "extendedRequestId": "J2tkPHvVIAMFoBg=",
 *	        "requestTime": "11/Jul/2018:08:42:15 +0000",
 *	        "path": "/dev/setissueduedatecreated",
 *	        "accountId": "605367074299",
 *	        "protocol": "HTTP/1.1",
 *	        "stage": "dev",
 *	        "requestTimeEpoch": 1531298535975,
 *	        "requestId": "5052acbe-84e6-11e8-8c87-79ab42881bbf",
 *	        "identity": {
 *	            "cognitoIdentityPoolId": null,
 *	            "accountId": null,
 *	            "cognitoIdentityId": null,
 *	            "caller": null,
 *	            "sourceIp": "34.214.71.183",
 *	            "accessKey": null,
 *	            "cognitoAuthenticationType": null,
 *	            "cognitoAuthenticationProvider": null,
 *	            "userArn": null,
 *	            "userAgent": null,
 *	            "user": null
 *	        },
 *	        "apiId": "muag43srvh"
 *	    },
 *	    "body": "{\"resource_url\":\"https://dynamo6.api.accelo.com/api/v0/issues/37400.json\",\"id\":\"37400\"}",
 *	    "isBase64Encoded": false
 * }
 */
 const ACCELO = require('d6-accelo/accelo')
 const CONSTANTS = require('./constants.js')
 const CRYPTO = require('crypto')
 const FS = require('fs')
 const LOGGER = require('d6-accelo/logger')
 const MOMENT = require('moment-timezone')
 const SHAREDCONSTANTS = require('d6-accelo/constants')
 const SLADATE = require('d6-accelo/sla-date')
 /**
  * @const {Array}
  */
 const SLA_DUE_DATE_HOURS = [6, 8, 16, null]

 const AWS = require('aws-sdk')
 AWS.config.update({region: "us-east-1"})
 const DDB = new AWS.DynamoDB.DocumentClient()

 MOMENT.tz.setDefault("UTC")

 let accelo = null
 let handlerCallback = null
 let logfile = null
 let ddb, s3, slaDate

/**
 * Process complete. Wraps the response with correct headers and statusCodes for API Gateway.
 * @param {Object} error
 * @param {Object} result
 * @param {Function} callback
 * @return {void}
 */
function done(error, result) {
	if(error)
	{
		handlerCallback(null, {
			statusCode: 500,
			body: JSON.stringify(error)
		})
	}
	else
	{
		handlerCallback(null, {
			statusCode: 200,
			body: JSON.stringify(result)
		})
	}
}
/**
 * Returns holidays stored s3 bucket
 * @return {array} list of sla holidays
 */
function loadHolidays(){
    return new Promise((resolve, reject)  => {
        if( process.env.NODE_ENV === 'production') {
            s3.getObject({
                Bucket: SHAREDCONSTANTS.SLA_BUCKET,
                Key: SHAREDCONSTANTS.SLA_HOLIDAYS + '.json'
            }, (error, data) => {
                if( null ===  error )
                {
                    resolve(data.Body.toString())
                }
                else
                {
                    reject(error)
                }
            })
        }
        else
        {
            resolve(JSON.parse(FS.readFileSync('./data/getSLAHolidays.json', 'utf-8')).data)
        }

    })
}
/**
 * Load the issue. If unsuccessful, then the lambda:done will be called to exit the function.
 * @private
 * @param {integer} id
 * @return {Promise}
 */
function loadIssue(accelo, issue_id) {

	LOGGER.log(CONSTANTS.MESSAGE_LOADING_ISSUE, LOGGER.LEVEL_INFO, issue_id, logfile)

    return new Promise((resolve, reject) => {
			accelo.getObject("issues/" + issue_id, {_fields: '_ALL'}, (error, result) => {
				if(null === error)
                {
					resolve({
						id: result.id,
						issue_status: result.issue_status,
						issue_priority: result.issue_priority,
						date_opened: result.date_opened ? result.date_opened : new MOMENT().tz('Pacific/Auckland').unix(),
						date_opened_nz: result.date_opened ? MOMENT.unix(result.date_opened).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : new MOMENT().tz('Pacific/Auckland').format('YYYY-MM-DD HH:mm:ss'),
						date_responded_nz: result.date_opened ? MOMENT.unix(result.date_opened).tz("Pacific/Auckland").format('YYYY-MM-DD HH:mm:ss') : new MOMENT().tz('Pacific/Auckland').format('YYYY-MM-DD HH:mm:ss')
					})
				}
				else
                {
					reject({
                        message: CONSTANTS.ERROR_LOADING_ISSUE,
                        data: issue_id,
                        error: error
                    })
				}
			})
		})
}
/**
 * Sets ticket due date, making sure the due date assigned is within working hours
 *
 * @param {object} ticket object with issue_id, issue_status, and issue_priority
 * @return {moment} 	  due date of the ticket
 */
function setDueDateOnCreated( slaDate, issue ) {
	let dateDue, dateDue_UTC,
		endpoint = 'issues',
		mDateOpened = new MOMENT(issue.date_opened_nz),
		mDateOpenedIsBusinessDay = slaDate.isBusinessDay(mDateOpened),
		slaInSeconds = SLA_DUE_DATE_HOURS[issue.issue_priority - 1] * 60 * 60

	let c = new MOMENT(MOMENT.unix(issue.date_opened))
	let d = new MOMENT(issue.date_opened_nz)
	let UTCToNZT = d.diff(c, 'seconds')

	// Issue opened before 8am will be set to 8am
	if( (mDateOpenedIsBusinessDay.isBusinessHours === false) &&
		(mDateOpenedIsBusinessDay.office_hours.is_office_hours === false) &&
		(mDateOpenedIsBusinessDay.office_hours.time_after === true) )
	{
		mDateOpened = slaDate.getNextBusinessDay(mDateOpened.add(1, 'days').hour(8).minute(0).seconds(0), 1, 'days')
	}

	// Ticket opened after 5pm will be set to 8am the following day
	if( (mDateOpenedIsBusinessDay.isBusinessHours === false)  &&
		(mDateOpenedIsBusinessDay.office_hours.is_office_hours === false) &&
		(mDateOpenedIsBusinessDay.office_hours.time_before === true) )
	{
		mDateOpened = slaDate.getNextBusinessDay(mDateOpened.hour(8).minute(0).seconds(0), 1, 'days')
	}
	dateDue = slaDate.assignNewDueDate( mDateOpened , slaInSeconds)
	dateDue_UTC = dateDue.clone().subtract(UTCToNZT, 'seconds')

	return new Promise((resolve, reject) => {
        accelo.update(endpoint, 'put', issue.id, {'date_due': dateDue_UTC.unix()}, (error, result) => {
            if( null === error ) {
                LOGGER.log(CONSTANTS.MESSAGE_SET_ISSUE_DUE_DATE, LOGGER.LEVEL_INFO, issue.id, logfile)
				issue.date_due_nz = dateDue
				issue.date_due_utc = dateDue_UTC
				issue.date_due = dateDue_UTC.unix()
                resolve( issue )
			}
			else {
                LOGGER.log(CONSTANTS.ERROR_UPDATE_ISSUE_DUE_DATE, LOGGER.LEVEL_INFO, issue.id, logfile)
				reject({
                    message: CONSTANTS.ERROR_UPDATE_ISSUE_DUE_DATE,
                    data: issue.id,
                    error: error
                })
			}
        })
	})
}
/**
 * Main handler function
 * @param  {Object}   event
 * @param  {Object}   context
 * @param  {Function} callback
 * @return {void}
 */
exports.handler =  function(event, context, callback) {

	try{

		const CONFIG = (event.credentials ? event.credentials : (process.env.NODE_ENV == "production" ? JSON.parse(process.env.CREDENTIALS) : JSON.parse(FS.readFileSync('.dev'))))
		const ISSUE_ID = JSON.parse(event.body).id

		accelo = (event.accelo ? event.accelo : new ACCELO(CONFIG.accelo))
        handlerCallback = callback
        logfile = event.logfile ? event.logfile : null
		s3 = (event.s3 ? event.s3 : new AWS.S3({apiVersion: '2006-03-01'}))

		// Authenticate
		var secret = (event.secret ? event.secret : process.env.SECRET)
		var authenticateToken = event.authenticateToken ? event.authenticateToken : accelo.authenticateToken;

		LOGGER.log(CONSTANTS.MESSAGE_AUTHENTICATING, LOGGER.INFO, {token: event.headers["X-Hub-Signature"], body: event.body}, logfile)

		if(!authenticateToken(secret, event.headers["X-Hub-Signature"], event.body))
		{
			return done({message: CONSTANTS.ERROR_AUTH, signature: event.headers["X-Hub-Signature"], body: event.body})
		}

		// A safeguard to limit updating
		if(process.env.ONLY_UPDATE_THIS_ISSUE && ISSUE_ID!=process.env.ONLY_UPDATE_THIS_ISSUE) return done({
			message: "Only "+process.env.ONLY_UPDATE_THIS_ISSUE+" issue will be updated",
			data: {
				onlyUpdateID: process.env.ONLY_UPDATE_THIS_ISSUE,
				eventIssueID: ISSUE_ID
			}
		})

		LOGGER.log("Connecting...", LOGGER.LEVEL_INFO, null, logfile)

		accelo.connect((error, result)=>{

            // Connection error
            if(error) return done({
                message: 'Connection error',
                error: error
            }, null)

            LOGGER.log("Connected", LOGGER.LEVEL_INFO, null, logfile)

            let issuePromise = loadIssue(accelo, ISSUE_ID),
                holidaysPromise = loadHolidays()

            Promise.all([issuePromise, holidaysPromise])
                .then( result => {
                    let issue = result[0]
                    let holidays = result[1]

                    if( process.env.NODE_ENV === 'production') {
                        holidays = JSON.parse( result[1] )
                        slaDate = new SLADATE(holidays.data)
                        // console.log(issue)
                        // console.log(holidays.data)
                    } else {
                        slaDate = new SLADATE(holidays)
                    }

					// Sets ticket due date for opened ticket
					if( issue.issue_priority != '4' && (issue.issue_status === '2' || issue.issue_status === '22'))
					{
                        setDueDateOnCreated( slaDate, issue )
							.then( result => {
								return done(null, result)
							})
							.catch( error => {
								console.log(error)
								return done(error)
							})
                    }
                    else
                    {
                        return done(null, {})
                    }
                })
                .catch(error => {
					console.log( error )
					return done(error)
				})
        })

	}
	catch (e) {
		done(e)
	}
};
