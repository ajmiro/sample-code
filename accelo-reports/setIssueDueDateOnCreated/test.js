'use strict'
// Constants
const EXPECT = require('chai').expect
const LAMBDA = require('./index.js')
const FS = require('fs')
const PATH = require('path')
const MOMENT = require('moment-timezone')
const CONSTANTS = require('./constants.js')
const CONTEXT = {functionName: PATH.basename(__dirname)}


process.env.NODE_ENV = 'test'

// The tests
describe("Lambda: " + CONTEXT.functionName, function() {

	let authenticateToken = () => true
	// Baseline mock accelo service
	let acceloMockService = {
		connect: (cb) => cb(),
		getObject: (endpoint, params, callback) => callback(null,  {
			id: '37396',
			issue_status: 2,
			issue_priority: '3',
			date_opened: 1532898337,
			date_submitted: 1532898336
		}),
		update: (endpoint, method, id, data, callback) => callback(null, {
			meta:{
				more_info: 'https://api.accelo.com/docs/#status-codes',
				message: 'Everything executed as expected.',
				status: 'ok'
			},
			response: {
				id: '37396',
				title: 'Opened ticket'
			}
		})
	}
	let S3MorkService = {
		getObject: (params, callback) => callback(null, {
			data: {
				Body: JSON.parse(FS.readFileSync('./data/getSLAHolidays.json', 'utf-8')).data
			}
		})
	}
	let event = {
	    "resource": "/set-issue-due-date-on-created",
	    "path": "/setissueduedatereopened",
	    "httpMethod": "POST",
	    "headers": {
	        "Content-Type": "application/json",
	        "Host": "muag43srvh.execute-api.us-east-1.amazonaws.com",
	        "X-Accelo-Event": "update_issue",
	        "X-Amzn-Trace-Id": "Root=1-5b58db4b-298eeaf856eb2b4a6824ece6",
	        "X-Forwarded-For": "54.200.89.239",
	        "X-Forwarded-Port": "443",
	        "X-Forwarded-Proto": "https"
	    },
	    "queryStringParameters": null,
	    "pathParameters": null,
	    "stageVariables": null,
	    "requestContext": {
	        "resourceId": "zgruqp",
	        "resourcePath": "/setissueduedatereopened",
	        "httpMethod": "POST",
	        "extendedRequestId": "KmczuFCWIAMFc6g=",
	        "requestTime": "25/Jul/2018:20:19:23 +0000",
	        "path": "/dev/setissueduedatereopened",
	        "accountId": "605367074299",
	        "protocol": "HTTP/1.1",
	        "stage": "dev",
	        "requestTimeEpoch": 1532549963087,
	        "requestId": "05021e98-9048-11e8-8db2-e3a5f7f3db43",
	        "identity": {
	            "cognitoIdentityPoolId": null,
	            "accountId": null,
	            "cognitoIdentityId": null,
	            "caller": null,
	            "sourceIp": "54.200.89.239",
	            "accessKey": null,
	            "cognitoAuthenticationType": null,
	            "cognitoAuthenticationProvider": null,
	            "userArn": null,
	            "userAgent": null,
	            "user": null
	        },
	        "apiId": "muag43srvh"
	    },
	    "body": "{\"resource_url\":\"https://dynamo6.api.accelo.com/api/v0/issues/37396.json\",\"id\":\"37396\"}",
	    "isBase64Encoded": false,
		// Extra TEST parameters
		accelo: Object.assign({}, acceloMockService),
		authenticateToken: authenticateToken,
		credentials: JSON.parse(FS.readFileSync('.dev')),
		logfile: './logs/' + CONTEXT.functionName + ".log",
		s3: Object.assign({}, S3MorkService),
		secret: ''
	}

	it('Should load issue successfully (mock)', function(done){
		this.timeout(30000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)

				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)

				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should exit gracefully if issue priority == 4, status != 2 or status = 22 (mock)', function(done){
		this.timeout(30000)
		event.accelo.getObject = (endpoint, params, callback) => callback(null,  {
			id: '37396',
			issue_status: '2',
			issue_priority: '4',
			date_opened: 1532898337,
			date_submitted: 1532898336
		}),
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)

				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)
				EXPECT(body).to.be.empty
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should set date_opened now() if status = 22 or date_opened = null (mock)', function(done){
		this.timeout(30000);
		event.accelo.getObject = (endpoint, params, callback) => callback(null,  {
			id: '37396',
			issue_status: '22',
			issue_priority: '3',
			date_opened: null,
			date_submitted: 1532898336
		}),
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)
				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)
				EXPECT(body.date_opened).to.not.equal(null)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should set issue successfully if issue priority != 4, status = 2 or status = 22 (mock)', function(done){
		this.timeout(30000);
		event.accelo.getObject = (endpoint, params, callback) => callback(null,  {
			id: '37396',
			issue_status: '2',
			issue_priority: '3',
			date_opened: 1532898337,
			date_submitted: 1532898336
		}),
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)

				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)

				done()
			} catch(e) {
				done(e)
			}
		})
	})

	let issues = JSON.parse(FS.readFileSync('./data/data-created.json', 'utf-8')).data
	issues.forEach((item, index) => {
		it('Should pass ' + item.issue_id + ' expected date due: ' + item.expected_date_due, function(done){
			this.timeout(10000);
			item.date_opened = new MOMENT(item.date_responded_utc)
			item.date_opened = item.date_opened.unix()
			event.accelo = Object.assign({}, acceloMockService)
			event.accelo.getObject = (endpoint, params, callback) => callback(null,  {
				id: item.issue_id,
				issue_status: item.issue_status,
				issue_priority: item.issue_priority,
				date_opened: item.date_opened
			})
			event.body = "{\"resource_url\":\"https://dynamo6.api.accelo.com/api/v0/issues/" + item.issue_id + ".json\",\"id\":\"" + item.issue_id + "\"}"
			LAMBDA.handler(event, {}, (error, result) => {
				try{
					let body = JSON.parse(result.body)
					let dateDueNZT = new MOMENT(body.date_due_nz)
					EXPECT(result.statusCode).to.equal(200)
					EXPECT(dateDueNZT.format('YYYY-MM-DD HH:mm:ss')).to.equal(item.expected_date_due)
					done()
				} catch(e) {
					done(e)
				}
			})
		})
	})

	/*it('Should set issue successfully if issue priority != 4, status = 2 or status = 22 (live)', function(done){
		this.timeout(30000)
		delete event.accelo
		event.body =  "{\"resource_url\":\"https://dynamo6.api.accelo.com/api/v0/issues/37396.json\",\"id\":\"37396\"}"
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				var body = JSON.parse(result.body)
				console.log(body)
				EXPECT(error).to.equal(null)
				EXPECT(result.statusCode).to.equal(200)

				done()
			} catch(e) {
				done(e)
			}
		})
	})*/

})
