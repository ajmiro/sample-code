module.exports = {
	ERROR_AUTH: "Invalid token",
	ERROR_LOADING_ISSUE: "Error loading issue",
	ERROR_UPDATE_ISSUE_DUE_DATE: "Error setting issue due date",
	MESSAGE_AUTHENTICATING: "Authenticating token",
	MESSAGE_LOADING_ISSUE: "Loading full issue",
	MESSAGE_SET_ISSUE_DUE: "Set issue due date in accelo",
	MESSAGE_SET_ISSUE_DUE_DATE: "Set issue due date successfully in accelo"
}
