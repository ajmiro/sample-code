import { Component, OnInit, OnDestroy, OnChanges, Input, Output, SimpleChange, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';
import { FormGroup, FormBuilder, Validators } 	from '@angular/forms';
import { CustomValidators }				from './../validators/custom-validators';
import { CustomValidationMessagePipe }	from './../pipes/custom-validation-message.pipe';
import { AppService }	from './../services/app.service';
import { HttpService }	from './../services/http.service';

/**
 * Decorators
 */
@Component({
	//	moduleId: module.id,
	selector: 'reset-password',
	templateUrl: '../templates/reset-password.component.html'
})
/**
 * Handles Reset Password Form Layout
 *
 * @author Aileen M
 * @version 1.0.0
 */
export class ResetPasswordComponent implements OnInit, OnDestroy, OnChanges{

    /**
	 * Group the values of each form fields into one object,
	 * with each field name as the key
	 * @type  {FormGroup}
	 */
	resetPasswordForm: FormGroup;

    /**
     * Set to true when form is submitted
     * @return {boolean}
     */
    loading: boolean = false;

    /**
     * Input from parent component to show reset password layout
     * @type {boolean}
     */
    @Input() resetPasswordShow: boolean;
    /**
     * Emits event to parent component to set resetPasswordShow to
     * false
     * @return {[type]} [description]
     */
    @Output() onResetPasswordToggle = new EventEmitter<boolean>();
	/**
	 * Class constructor
	 *
	 * @param  {Router}       router
	 * @param  {FormBuilder}  fb
	 * @return {ResetPasswordComponent}
	 */
	constructor(
        private appService: AppService,
		private fb: FormBuilder,
		private httpService: HttpService,
		private router: Router
	) {

    }


	/**
	 * Default method run after class is instantiated
	 *
	 * @return {void}
	 */
	ngOnInit(): void {
        this.buildForm();
    }

    /**
     * Detect simple changes from parent component
     * @param  {SimpleChange} changes
     *
     * @return {SimpleChnage}
     */
    ngOnChanges(changes: {[propKey: string]: SimpleChange}) : void {
        console.log( changes );
        for(let propName in changes )
        {
            if( propName === 'resetPasswordShow' )
            {

            }
        }
    }

	/**
	 * Unsubscribe any subscripton to prevent memory leaks
	 *
	 * @return {void}
	 */
	ngOnDestroy() : void {

	}

	/**
	 * build form
	 *
	 * @return {void}
	 */
    buildForm() : void {
        this.resetPasswordForm = this.fb.group({
			password : ['', Validators.required],
			password_new : ['', Validators.compose([
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(16),
				CustomValidators.isEqual('password_confirmation')
			])],
			password_confirmation : ['', Validators.compose([
				Validators.required,
				CustomValidators.isEqual('password_new')
			])]
		});
    }

    /**
     * Reset password hide
     *
     */
    resetPasswordToggle( show : boolean) {
        this.onResetPasswordToggle.emit( show );
        this.resetPasswordShow = false;
        this.resetPasswordForm.reset();
    }

    /**
     * Update password
     * @return {void}
     */
    update() : void {
		let data = this.resetPasswordForm.value;
		let url = this.appService.config.endpoints.resetPasswordAuth.replace(':stub', sessionStorage.getItem('userStub') );
		this.loading = true;
		this.httpService.sendRequest( url, data, 'put' )
			.then( (response:any) => {
				let json = response.json();
				this.appService.addNotification( json.result, 'success', '', '', 5000);
				this.loading = false;
	            this.resetPasswordToggle( false );
	            this.appService.logout();
			})
			.catch( (error:any ) => {
				let json = error.json();
				this.appService.addNotification( json.error ,  'error', '', '', 3000);
				this.loading = false;
			});
    }
}
