import { Injectable }       from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad, Router,
  Route, ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';
import { AppService }      from './../services/app.service';
import { HttpService }      from './../services/http.service';

/**
 * AuthGuard Class - authorised user can directly visit url. If not then user will
 * be redirected to login page.
 * @author Aileen M
 * @version 1.0
 *
 */
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

    /**
     * Default method to initialise class
     * @param  {AppService}  appService
     * @param  {Router}      router
     * @param  {HttpService} httpService
     * @return {AuthGuard}
     */
    constructor(private appService: AppService, private router: Router, private httpService: HttpService){}

    /**
    *   Only activates route when user is authorised
    *
    * @param {ActivatedRouteSnapshot} route
    * @param {RouterStateSnapshot} state
    *
    * @return {boolean}
    **/
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        return this.checkLogin(url);
    }

    /**
    *  Only activates child route when user is authorised
    *
    * @param {ActivatedRouteSnapshot} route
    * @param {RouterStateSnapshot} state
    *
    * @return {boolean}
    **/
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    /**
    * Will load component if user is authorised
    *
    * @param {Route} route
    *
    * @return {boolean}
    **/
    canLoad(route: Route): boolean {
      let url = `/${route.path}`;

      return this.checkLogin(url);
    }

    /**
     * Checks if user is authorised or not
     *
     * @param  {string}  url
     * @return {boolean}
     */
    checkLogin(url: string): boolean {
        // Store the attempted URL for redirecting
        this.appService.redirectUrl = url;

        //create a promise here to check with API is session is still existing.
        if( (sessionStorage.getItem('loggedIn') === 'true') )
        {
            this.appService.setLoggedIn(true);
            return true;
        }

        // Navigate to the login page with extras
        this.router.navigateByUrl('/login');
        return false;
    }
}
