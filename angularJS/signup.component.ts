import { Component, OnInit, OnDestroy } 						from '@angular/core';
import { FormGroup, FormBuilder, Validators } 	from '@angular/forms';
import { Router, ActivatedRoute } 								from '@angular/router';
import { User } 								from './../models/user';
import { AppService }							from './../services/app.service';
import { HttpService }							from './../services/http.service';
import { SignupService } from './../services/signup.service';
import { Subscription } from "rxjs";

import { CustomValidators }				from './../validators/custom-validators';

import { AccountDetailsComponent } from './../components/account-details.component';
import { SubscriptionComponent } from './../components/subscription.component';
/**
 * Decorators
 */
@Component({
	//	moduleId: module.id,
	selector: 'signup',
	templateUrl: '../templates/signup.component.html'
})
/**
 * SignupComponent - Displays Signup panel
 * @author  Aileen Miro
 * @version 1.0
 */
export class SignupComponent implements OnInit, OnDestroy{
	/**
	 * Code form to capture email code entered by user
	 * @type {FormGroup}
	 */
	codeForm: FormGroup;

	/**
	 * Sets to the current step the user with reference
	 * to the wizard
	 * @type {number}
	 */
	currentStep = 0;


	/**
	 * Set to true once form is valid in child component to go to next step
	 * @type {boolean}
	 */
	isValid: boolean = false;

	/**
	 * Sets to true when form is submitted
	 * @type  {boolean}
	 */
	loading: boolean = false;

	/**
	 * subscription to any changes
	 * @type {Subscription}
	 */
	subscription: Subscription;

	/**
	 * Store the user's input temporarily and display it
	 * on review and finish section
	 *
	 * @type {any}
	 */
	userTempData: any = {};

	/**
	 * User data return after validates email
	 * @return {User}
	 */
	user: User = new User();

	/**
	 * Dummy details for the signup information. This information
	 * will come from the CMS API later on
	 * @return {any}
	 */
	signupJson = {
		    "steps_count": "4",
		    "steps_list": [
				{
					"step_id": "account_details",
		            "title": "Account <br>Details"
		        },
				{
					"step_id": "validate_email",
					"title": "Validate<br> Email"
				},
				{
					"step_id": "payment_details",
					"title": "Payment <br>Details",

				},
		        {
					"step_id": "review_confirm",
		            "title": "Review <br>and Finish"
		        }
			]
		};

	/**
	 * Default method initiated to intiates classes and subclasses needed
	 * @param  {ActivatedRoute} route
	 * @param  {SignupService}  signupService  Connects to api @REDO
	 * @param  {AppService}  appService  Global service to set global variable
	 * @param  {Router}      router      Used to navigate from one page to another
	 * @return {SignupComponent}
	 */
	constructor(
		private route: ActivatedRoute,
		private appService : AppService,
		private fb: FormBuilder,
		private httpService: HttpService,
		private router: Router,
		private signupService: SignupService) {}

	/**
	 * Method is called after component is created.
	 *
	 * @return {void}
	 */
	ngOnInit(): void {
		// Given that user did not use the magic link to confirm email
		this.codeForm = this.fb.group({
			activation_code: ['', Validators.compose([
				Validators.required])]
		});

		this.codeForm.statusChanges.subscribe(() => {
			this.isValid = this.codeForm.valid;
		});

		this.subscription = this.route.params.subscribe( params => {
			for( let key in params )
			{
				if( key === 'token')
				{
					this.currentStep++;
					this.codeForm.setValue({activation_code: params[key]});
				}
			}
		});
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	/**
	 * Submits valid form and updates user on successful registration
	 *
	 * @return {void}
	 *
	 */
	navigateWizard( dir : string ): void {

		switch( this.currentStep )
		{
			case 0:
				this.signup();
				break;

			case 1:
				this.validateEmailToken();
				break;
			case 2:
				this.paymentDetails();
				break;

			case 3:
				this.confirmSubscription();
				break;

			default:
				break;
		}
	}

	/**
	 * Sets userTempData from AccountDetailsComponent form if form is valid and
	 * enable next button
	 *
	 * @param {FormGroup} formData
	 */
	onSaveAccountDetails( formData : FormGroup ) : void {
		this.isValid = formData.valid;
		if( this.isValid )
		{
			for( let key in formData.value )
			{
				this.userTempData[key] = formData.value[key];
			}
		}
	}

	/**
	 * Sets userTempData subscription base on the users
	 * selection from SubscriptionComponent
	 *
	 * @param {any} subscriptionPlan
	 */
	onSubscriptionSelectPlan( subscriptionPlan : any ) : void {
		this.isValid = true;

		// @TODO: Data should be coming from chargify
		this.userTempData['product_handle'] = 'small-plan';
	}

	/**
	 * Save payment details to chargify
	 */
	onSaveBillingInformation( billingInformation: any ) : void {
		this.isValid = billingInformation.valid;
		if( this.isValid )
		{
			this.userTempData['billing_information'] = billingInformation.value;
		}

	}

	/**
	 * Connects to API to create subscription.
	 *
	 * @return {void}
	 */
	confirmSubscription() : void {
		this.loading = true;
		let data = {
			'customer_id': this.userTempData['customer_id'],
			'payment_profile_id': this.userTempData['payment_profile_id'],
			'product_handle': this.userTempData['product_handle']
		};

		this.signupService.createSubscription( data )
			.then( (response:any) => {
				this.router.navigateByUrl('/dashboard');
			})
			.catch( error => {
				this.errorHandler( error );
			})

	}

	/**
	 * Connect to api for payment details
	 *
	 */
	paymentDetails() :void {
		this.loading = true;

		let data = this.userTempData['billing_information'];  // billing information detects changes from here but
		data['customer_id'] = this.userTempData['customer_id'];

		this.signupService.createPaymentDetails( data )
			.then( (response: any) => {
				let json = response.json();
				this.userTempData['payment_profile_id'] = json.id;
				this.currentStep++;
				this.isValid = true;
				this.loading = false;
				console.log( this.currentStep );
			})
			.catch( error => {
				console.log( 1 );
				this.errorHandler( error );
			})
	}

	/**
	 * User requests to resend email verification link
	 * @return {void}
	 */
	resendEmailVerification() : void {
		this.loading = true;
		let url = this.appService.config.endpoints.resendEmailVerification;
		let data: any;
		console.log( this.codeForm.value.activation_code );
		this.loading = false;
		if( this.codeForm.value.activation_code !== '' )
		{
			data = { email_token: this.codeForm.value.activation_code }
		}
		else
		{
			data = { email: this.userTempData['email'] }
		}

		this.httpService.sendRequest( url, data, 'put')
			.then( (response: any ) => {
				let json = response.json();
				this.appService.addNotification( json.result, 'success', '', '', 5000);
				this.loading = false;
			})
			.catch( error => {
				this.errorHandler( error );
				this.loading = false;
			});
	}

	/**
	 * Save user details temporarily for trial options and let
	 * user confirm email address if then did not proceed
	 * signup for subscription
	 *
	 * Should be able to save the information form account details form
	 * to be use for the finish and review section on step 4
	 *
	 * @return {void}
	 */
	signup() : void {

		this.signupService.signup( this.userTempData )
			.then( res => {

				this.appService.addNotification('You have successfully signed up for an account. Please check your email address for confirmation.', 'success', '', '', 5000);

				this.currentStep++;
				this.isValid = false;
				this.loading = false;
			})
			.catch( error => {
				this.errorHandler( error );
			});
	}

	/**
	 * Submits email token user provided. It'll proceed to add next step
	 * if valid or prompt user with invalid token provided.
	 *
	 * It should store the customer_id to be used to the next step for
	 * creating a payment profile
	 *
	 * @return {void}
	 */
	validateEmailToken() : void {

		this.loading = true;
		this.signupService.validateCode(  this.codeForm.value.activation_code )
			.then( (response:any) => {
				this.userTempData = response.json();
				this.userTempData['product_handle'] = 'small-plan';
				this.currentStep++;
				this.isValid = true;
				this.loading = false;
			})
			.catch( error => {
				// @TODO: CODE-12 Standardise error format from the backend
				if( error.status === 404)
				{
					let json = error.json();
					this.appService.addNotification( json.error.error, 'error', '', '', 10000);
					this.isValid = true;
					this.loading = false;
				}
				else
				{
					this.errorHandler( error );
				}

			});

	}

	errorHandler( error ) : void {
		let json = error.json();
		let message = json.error || error;

		this.appService.addNotification( message, 'error', '', '', 10000);
		this.isValid = true;
		this.loading = false;
	}


}
