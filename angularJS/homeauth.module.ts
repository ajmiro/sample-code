import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


// Custom Modules
// Validation Form Validation
import { AppValidationModule }		from './app-validation.module';

import { AppService } from './../services/app.service';
import { HttpService } from './../services/http.service';
import { PopupService } from './../services/popup.service';
import { SignupService } from './../services/signup.service';

import { AccountDetailsComponent }    from './../components/account-details.component';
import { ForgotPassComponent } from './../components/forgotpass.component';
import { HomeComponent } from './../components/home.component';
import { LoginComponent } from './../components/login.component';
import { SignupComponent }    from './../components/signup.component';
import { SubscriptionComponent } from './../components/subscription.component';
import { BillingInformationComponent } from './../components/billing-information.component';

import { CCFormatDirective } from './../directives/cc-format.directive';

const homeAuthRoutes: Routes = [
    // Auth routes
    {
        path: 'public/forgotten-password',
        component: HomeComponent,
        children: [
            {
                path: '**',
                redirectTo: 'public/forgotten-password'
            }
        ]
    },
    {
        path: 'public/reset-password',
        redirectTo: 'forgotten-password'
    },
    {
        path: 'public/reset-password/:token',
        component: HomeComponent,
    },
	{	path: 'public/login',
		component: HomeComponent
	},
    {
        path: 'public/signup',
        component: HomeComponent
    },
    {
        path: 'public/signup/:token',
        component: HomeComponent,
    }

];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AppValidationModule,
        RouterModule.forChild(homeAuthRoutes)
    ],
    declarations: [
        AccountDetailsComponent,
        ForgotPassComponent,
        HomeComponent,
        LoginComponent,
        SignupComponent,
        SubscriptionComponent,
        BillingInformationComponent,

        CCFormatDirective
    ],
    providers: [
        AppService,
        HttpService,
        PopupService,
        SignupService
    ]
})
export class HomeAuthModule {}
