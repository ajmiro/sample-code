import { Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Validator, AbstractControl, NG_VALIDATORS, ValidatorFn } from '@angular/forms';
import { SignupService } from './../services/signup.service';

import { CustomValidators } from './../validators/custom-validators';

export const VALID_CODE_VALIDATOR: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => ValidCodeValidator ),
    multi: true
}

@Directive({
    selector: '[validateCode][formControlName],[validateCode][formControl],[validateCode][ngModel]',
    providers: [ VALID_CODE_VALIDATOR ],
    host: {'[attr.validateCode]': 'validateCode ? validateCode : null'}
})

export class ValidCodeValidator implements Validator {
    private _validator: ValidatorFn;
    private _onChange: () => void;
    private customValidator: CustomValidators;

    @Input() validateCode: string /* password field */;

    constructor(private signupService: SignupService){}

    ngOnChanges(changes: SimpleChanges): void {
        if ('validateCode' in changes) {
          this._createValidator();
          if (this._onChange) this._onChange();
        }
    }

  validate(c: AbstractControl): {[key: string]: any} {
      return this._validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
      this._onChange = fn;
  }

  private _createValidator(): void {
      this._validator = CustomValidators.findEmailToken( this.signupService );
  }
}
