<?php
namespace app\models\capsule;

class CapsuleHistory extends CapsuleBase {

    public $belongsTo = array(
        'Organisation'=>array(
            'to'=>'app\models\capsule\CapsuleOrganisation',
            'key'=>array(
                'partyID'=>'id'
            )

        ),
        'Opportunity'=>array(
            'to'=>'app\models\capsule\CapsuleOpportunity',
            'key'=>array(
                'opportunityId'=>'id'
            )

        ),
        'Person'=>array(
            'to'=>'app\models\capsule\CapsulePerson',
            'key'=>array(
                'partyID'=>'id'
            )

        )
    );
    public static function userVisits($username, $startDate=null, $endDate=null) {

        $conditions = array(
            'note LIKE "%visit%"',
            'creator'=>$username
        );
        if(isset($startDate) && isset($endDate)) {

            $conditions[] = sprintf('entryDate >= "%s"',$startDate);
            $conditions[] = sprintf('entryDate <= "%s"',$endDate);
        }
        $visits = CapsuleHistory::find('all',array(
                'conditions'=>$conditions,
                'with'=>array('Organisation','Opportunity','Person')
            ));
        return $visits;
    }



}

?>