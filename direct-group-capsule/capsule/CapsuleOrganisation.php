<?php
namespace app\models\capsule;

use app\models\capsule\CapsuleHistory;
use app\models\capsule\CapsuleContact;

class CapsuleOrganisation extends CapsuleBase
{

    public $hasMany  = array(
        'History'=>array(
            'to'=>'app\models\capsule\CapsuleHistory',
            'key'=>'partyId'
        ),
        'Contact'=>array(
            'to'=>'app\models\capsule\CapsuleContact',
            'key'=>array(
                'id'=>'partyId'
            )

        ),
        'People'=>array(
            'to'=>'app\models\capsule\CapsulePerson',
            'key'=>array(
                'id'=>'organisationId'
            )
        )
    );

    public function createdWithin($entity, $startDate, $endDate)
    {

        $clientCreated =date('Y-m-d',strtotime($entity->createdOn));
        $startDate = $startDate;
        $endDate = $endDate;

        return $clientCreated >= $startDate && $clientCreated <= $endDate;
    }

    public function visits($entity, $startDate=null, $endDate=null) {
        $conditions = array(
            'partyId'=>$entity->id,
            'type'=>'Note',
            'note LIKE "%visit%"'
        );
        if(isset($startDate) && isset($endDate)) {

            $conditions[] = sprintf('entryDate >= "%s"',$startDate);
            $conditions[] = sprintf('entryDate <= "%s"',$endDate);
        }
        return CapsuleHistory::find('all',array(
            'conditions'=>$conditions
        ))->to('array');
    }

    public function getContact($entity, $contactType, $type=null) {

        $conditions = array(
            'partyId'=>$entity->id,
            'contact_type'=>$contactType
        );
        if($type) {
            $conditions['type'] = $type;
        }
        $contact = CapsuleContact::find('first', array(
            'conditions'=>$conditions
        ));
        return $contact;
    }

    public static function sync(\DateTime $lastModified)
    {
        //CapsuleBase::syncLog('info', 'Starting import process for organisations, last modified '.$lastModified->format('Y-m-d H:i:s'));

        $organisations = json_decode( CapsuleBase::getV2('parties', array(
            'since' => CapsuleBase::formatDate($lastModified),
            'embed' => 'fields'
        )));
        //
        $sync = array();
        $customFields = array();

        if( isset($organisations->parties ) ) {

            foreach($organisations->parties as $org ) {
                // only process type = organisation
                if( $org->type == 'organisation') {
                    $tempOrganisation = array(
                        'id'            => $org->id,
                        'name'          => $org->name,
                        'pictureURL'    => $org->pictureURL,
                        'customFields'  => null,
                        'createdOn'     => date('Y-m-d H:i:s',strtotime( $org->createdAt )),
                        'updatedOn'     => date('Y-m-d H:i:s',strtotime( $org->updatedAt )),
                    );

                    $capsuleOrganisation = CapsuleOrganisation::findById( $org->id );

                    // Update existing organisation
                    if( $capsuleOrganisation ) {
                        CapsuleBase::syncLog('info','Existing Organisation '. $org->id .'. Updating instead.');
                        $capsuleOrganisation->save( $tempOrganisation );
                        CapsuleBase::syncLog('info','Updated Organisation '. $org->id );
                    // Create a new organisation
                    }else{
                        CapsuleBase::syncLog('info','Importing Organisation '. $org->id );
                        $capsuleOrganisation = CapsuleOrganisation::create();
                        $capsuleOrganisation->save( $tempOrganisation );
                        CapsuleBase::syncLog('info', 'Imported Organisation '. $org->id );
                    }

                    $sync[] = $capsuleOrganisation->id;

                    // Set and save organisaton custom fields
                    self::saveContacts( $org );
                    self::saveCustomFields( $org );
                    self::saveHistory( $org );


                } // end type = organisation

            }

            if( count($sync) > 0){
                return CapsuleOrganisation::find('all', array(
                        'conditions'=>array(
                            'CapsuleOrganisation.id'=> $sync
                        ),
                        'with'=>array('Contact')
                ));
            }
        }



    }
    /**
     * History is now referred to as Entries in V2
     * @params  id      the organisation id
     * @return void
     */
    protected static function saveHistory( $org )
    {
        $uri = 'parties/' . $org->id . '/entries';
        $histories = json_decode( CapsuleBase::getV2($uri, array(
            'embed' => 'party'
        )));

        if( isset( $histories->entries ) ) {

            foreach( $histories->entries  as $history ) {

                $tempHistory = array(
                    'id'            => $history->id,
                    'type'          => $history->type,
                    'creator'       => $history->creator->username,
                    'creatorName'   => $history->creator->name
                );
                $capsuleHistory = CapsuleHistory::findById($history->id);

                if( isset($history->entryAt) ) {
                    $tempHistory['entryDate'] = date('Y-m-d H:i:s',strtotime($history->entryAt ));
                }

                if( isset($history->attachments) ) {
                    $tempHistory['attachments'] = serialize( $history->attachments );
                }
                if( isset($history->participants) ) {
                    $tempHistory['participants'] = serialize( $history->participants );
                }
                if( isset($history->subject) ) {
                    $tempHistory['subject'] = $history->subject;
                }
                if( isset($history->content) ) {
                    $tempHistory['content'] = $history->content;
                }
                if( isset($history->party ) ) {
                    $tempHistory['partyId'] = $history->party->id;
                    $tempHistory['partyName'] = $history->party->name;
                }

                if($capsuleHistory){
                   CapsuleBase::syncLog('info', 'Updating history '. $history->id);
                   $capsuleHistory->save( $tempHistory );
                }else{
                   CapsuleBase::syncLog('info', 'Creating history '. $history->id);
                   $capsuleHistory = CapsuleHistory::create();
                }
            }
        }

    }

    /**
     * Saving contacts
     * @return
     */
    protected static function saveContacts( $org )
    {
        if(isset( $org->phoneNumbers ) && count( $org->phoneNumbers ) > 0) {
            foreach( $org->phoneNumbers as $phoneNum ) {
                $capsulePhoneNumbers = CapsuleContact::findById( $phoneNum->id );
                $tempContact = array(
                   'id'                => $phoneNum->id,
                   'partyId'           => $org->id,
                   'contact_type'      => 'phone',
                   'type'              => $phoneNum->type,
                   'phoneNumber'       => $phoneNum->number
                );

                if( $capsulePhoneNumbers ) {
                    CapsuleBase::syncLog('info','Existing contact '. $phoneNum->id .'. Updating instead.');
                    $capsulePhoneNumbers->save( $tempContact );
                } else {
                     $capsulePhoneNumbers = CapsuleContact::create();
                     $capsulePhoneNumbers->save( $tempContact );

                }
            }

        } //end phone numbers

        if(isset( $org->emailAddresses ) && count( $org->emailAddresses ) > 0) {
            foreach( $org->emailAddresses as $emailAdd ) {
                $capsuleEmailAddresses = CapsuleContact::findById( $emailAdd->id );
                $tempContact = array(
                    'id'                => $emailAdd->id,
                    'partyId'           => $org->id,
                    'contact_type'      => 'email',
                    'type'              => $emailAdd->type,
                    'emailAddress'      => $emailAdd->address
                );
                if( $capsuleEmailAddresses ) {
                    $capsuleEmailAddresses->save( $tempContact );
                }else {
                    $capsuleEmailAddresses = CapsuleContact::create();
                    $capsuleEmailAddresses->save( $tempContact );
                }
            }

        } //end email addresses

        if(isset( $org->websites ) && count( $org->websites ) > 0 ) {
            foreach( $person->websites as $website ) {
                $tempContact = array(
                    'id'                => $website->id,
                    'partyId'           => $org->id,
                    'contact_type'      => null,
                    'type'              => $website->type,
                    'webAddress'        => $website->address,
                    'webService'        => $website->service,
                    'url'               => $website->url
                );
                $capsuleWebsites = CapsuleContact::findById( $website->id );
                if( $capsuleWebsites ) {
                    $capsuleWebsites->save( $tempContact );
                }else {
                    $capsuleWebsites = CapsuleContact::create();
                    $capsuleWebsites->save( $tempContact );
                }
            }
        } //end websites
    }

    /**
     * Saving custom fields
     * @return
     */
    protected static function saveCustomFields( $org )
    {
        $customFields = array();
        if( isset( $org->fields ) &&  count( $org->fields ) > 0) {
            $organisation = CapsuleOrganisation::findById( $org->id );

            if( $organisation ){
                CapsuleBase::syncLog('info', 'Saving custom fields for Opportunity '. $org->id );

                foreach( $org->fields as $field ){
                    array_push( $customFields, array(
                        'id' => $field->id,
                        'label' => $field->definition->name,
                        'text'  => $field->value
                    ));
                }

                $organisation->customFields = serialize( $customFields );
                $organisation->save();
             }
        }
    }
}

?>
