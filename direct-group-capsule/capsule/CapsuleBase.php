<?php
namespace app\models\capsule;

use Guzzle\Http\Client;
use Guzzle\Http\Plugin\CurlAuthPlugin;
use lithium\security\Auth;
use lithium\analysis\Logger;
use Guzzle\Common\Batch\BatchBuilder;
use \Curl\Curl;

class CapsuleBase extends \lithium\data\Model
{
    /** @var Client */
    private static $client;
    private static $ACCESS_TOKEN = "xxxx";
    private static $BASE_API_LINK = "https://api.capsulecrm.com/api/v2/";


    /**
     * Redo this to connect to
     * Returns the connection to Capsule API
     * @return \Guzzle\Http\Client
     */
    protected static function getClient()
    {
        if(!self::$client) {
            if ( $user = Auth::check('default') ) {

                self::$client = new Client('https://directgroup.capsulecrm.com/api/',array(

                    'curl.CURLOPT_SSL_VERIFYHOST' => false,
                    'curl.CURLOPT_SSL_VERIFYPEER' => false,
                ));


                self::$client->setDefaultHeaders(array(
                    'token' => $user['api_key'],
                    'Accept'=>'application/json',
                    'Content-Type'=>'application/json'
                ));

                // CapsuleBase::syncLog('info', 'Headers set.');

                self::$client->addSubscriber(new CurlAuthPlugin($user['api_key'], 'x'));

            } else {
                // CapsuleBase::syncLog('info', 'Session not authenticated.');
                throw new \Exception("Session not authenticated.");
            }
        }

        return self::$client;
    }

    /**
     * DEPRECATED AND USE getV2 instead
     * Initiate a GET request and returns the response in array format
     * @param $request string The URL to the resource
     * @return mixed
     */
    protected static function get($request)
    {

        try {
            $result = self::getClient()->get($request)->send();
        } catch (\Exception $e) {
            CapsuleBase::syncLog('info', 'Exception: ' . $e->getMessage());
        }

        if ($result) {
            CapsuleBase::syncLog('info', 'Successfully retrieved data.');
            return json_decode($result->getBody(true), true);
        }

    }


    protected static function getV2($request, $params)
    {
        $headers = array(
            "Authorization" =>  "Bearer " . self::$ACCESS_TOKEN,
            "Accept" =>  "application/json" ,
            "Cache-control" =>  "no-cache" ,
            "Content-Type" =>  "application/json"
        );

        $curl = new Curl();
        $curl->setHeaders( $headers );
        $curl->setOpt( CURLOPT_SSL_VERIFYHOST, false );
        $curl->setOpt( CURLOPT_SSL_VERIFYPEER, false );
        $curl->setOpt( CURLOPT_CUSTOMREQUEST, 'GET');
        $curl->setUrl('https://api.capsulecrm.com/api/v2/' . $request, $params);

        $curl->exec();

        if ($curl->error) {
            echo 'https://api.capsulecrm.com/api/v2/' . $request;
            echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
        } else {
            CapsuleBase::syncLog('info', 'Successfully retrieved data.');
            return json_encode( $curl->response, true);
        }

        //$curl->close();
    }
    /**
     * Initiate a batch GET request and call a callback function
     * @param array $urls
     * @param $callback
     */
    protected static function batchGet(array $requests, $callback)
    {
        $batch = BatchBuilder::factory()
            ->autoFlushAt(100)
            ->transferRequests(50)
            ->notify($callback)
            ->build();
        foreach($requests as $r) {
            $batch->add($r);
        }
        $batch->flush();
    }

    protected static function formatDate($date)
    {
        if(is_string($date) || $date instanceof \DateTime) {
            if (is_string($date)) {
                $date = new \DateTime(strtotime($date), new \DateTimeZone('Pacific/Auckland'));
            }

            return $date->format('Y-m-d').'T'.$date->format('H:i:s').'Z';
        }

    }

    public static function syncLog($priority, $message)
    {
        return Logger::write($priority, $message, array('name'=>'sync'));
    }
}


?>
