<?php
namespace app\models\capsule;

use app\models\capsule\CapsuleHistory;

class CapsuleOpportunity extends CapsuleBase {

    public $hasOne = array(
        'JobSheet'=>array(
            'to'=>'app\models\JobSheet',
            'key'=>array(
                'id'=>'opportunity_id'
            )

        )
    );

    public $belongsTo = array(
        'Organisation'=>array(
            'to'=>'app\models\capsule\CapsuleOrganisation',
            'key'=>array(
                'partyID'=>'id'
            )

        )
    );

    public function getCustomField($entity,$fieldName)
    {

        $customFields = $this->getCustomFields($entity);

        if(is_array($customFields)) {
            foreach ($customFields as $cF) {
                if($cF['label'] == $fieldName ) {
                    return $cF['text'];
                }
            }
        }
        return false;

    }

    public function getCustomFields($entity)
    {
        $customFields = array();
        if(!empty($entity->customFields)) {
            $customFields = unserialize($entity->customFields);
        }
        return $customFields;

    }

    public function visits($entity, $startDate=null, $endDate=null)
    {
        $conditions = array(
            'opportunityId'=>$entity->id,
            'type'=>'Note',
            'note LIKE "%visit%"'
        );
        if(isset($startDate) && isset($endDate)) {

            $conditions[] = sprintf('entryDate >= "%s"',$startDate);
            $conditions[] = sprintf('entryDate <= "%s"',$endDate);
        }
        return CapsuleHistory::find('all',array(
            'conditions'=>$conditions
        ))->to('array');
    }

    public static function sales($username, $startDate, $endDate)
    {
        $startDateFilter = $startDate.' 00:00:00';
        $endDateFilter   = $endDate.' 23:59:59';

        $report = array();
        $sales = 0;
        $quoted = 0;

        /**
         * Deal with opportunities
         */
        $o = CapsuleOpportunity::find('all',array(
                'conditions'=>array(
                    'CapsuleOpportunity.owner'=>$username,
                    'or'=>array(
                        sprintf('CapsuleOpportunity.actualCloseDate BETWEEN "%s" AND "%s"',$startDateFilter, $endDateFilter),
                        sprintf('CapsuleOpportunity.createdOn BETWEEN "%s" AND "%s"',$startDateFilter, $endDateFilter)
                    )
                )
        ));

        $visitIDs = array(); //For duplicate checking

        foreach($o as $i=>$record) {
            $createdOn = date('Y-m-d',strtotime($record->createdOn));

            $actualCloseDate = date('Y-m-d',strtotime($record->actualClosedate));

            $isClosedWon = $record->milestone == 'Closed Won';

            $isQuoted = ($record->milestone == 'Quoted' || $record->milestone == 'New');

            if( $isClosedWon || $isQuoted ) {

                $record->organisation = CapsuleOrganisation::findById($record->partyId);

                if(!$record->organisation) {
                    //todo: Log missing parties
                    continue;
                }

                $record->job_sheet = \app\models\JobSheet::findByOpportunityId($record->id);

                // skip deleted sheets
                if($record->job_sheet->is_deleted) {
                    continue;
                }

                $row = array();
                $row['type'] = 'sale';
                $row['data'] = $record;
                $row['name'] = $record->organisation->name;
                $row['sale_n'] = $record->organisation->createdWithin($startDate,$endDate);
                $row['sale_e'] = !$row['sale_n'];
                $row['source'] = $record->getCustomField('Source')?$record->getCustomField('Source'):'';
                $row['sample'] = $record->job_sheet->sample;
                $row['visit_c'] = $record->organisation->visits($startDateFilter,$endDateFilter);
                $row['visit_o'] = $record->visits($startDateFilter,$endDateFilter);

                foreach($row['visit_o'] as $v) {
                    $visitIDs[] =  $v['id'];
                }
                foreach($row['visit_c'] as $v) {
                    $visitIDs[] =  $v['id'];
                }
                $row['quoted'] = $row['sale'] = 0;

                if($isClosedWon){
                    $row['sale'] = $record->value;
                    $sales +=  $row['sale'];
                    $row['date'] =  $record->actualCloseDate;

                }
                if($isQuoted){
                    $row['quoted'] = $record->value;
                    $quoted +=  $row['quoted'];
                    $row['date'] =  $record->createdOn;


                }

                $row['sale'] = $record->value;
                $report[] = $row;

                $record = null;

            }
        }
        $o = null;

        $visits = CapsuleHistory::userVisits($username, $startDateFilter, $endDateFilter);

        $visits->each( function($record) use ($startDateFilter, $endDateFilter, $startDate, $endDate, &$report, &$visitIDs) {

                // Only include those not present in sales visits

                if(!in_array($record->id,$visitIDs)) {
                    $row = array();
                    $row['data'] = $record;
                    $row['type'] = 'visit';
                    if(!empty($record->organisation->id)) {
                        $row['name']  =  $record->organisation->name;
                    }
                    if(!empty($record->person->id)) {
                        $row['name']  =  $record->person->name().' at '.$record->person->organisationName;
                    }
                    $row['sale_n'] = $record->organisation->createdWithin($startDate,$endDate);
                    $row['sale_e'] = !$row['sale_n'];
                    $row['source'] = '';
                    $row['sample'] = '';
                    $row['visit_c'] = array($record->to('array'));

                    $row['quoted'] = $row['sale'] = 0;

                    $row['date'] = $record->entryDate;
                    $report[] = $row;
                }

            return $record;
        });

        usort($report,function( $a, $b ) {
            return strtotime($a["date"]) - strtotime($b["date"]);
        });
        return array(
            'sales'=>$sales,
            'quoted'=>$quoted,
            'report'=>$report

        );

    }

    /**
     * Fetch new opportunities in Capsule and save them in the database.
     * @param $lastModified
     * @return \lithium\data\collection\RecordSet The fetched records
     */
    public static function sync(\DateTime $lastModified)
    {
        CapsuleBase::syncLog('info', 'Starting import process for opportunity, last modified '.$lastModified->format('Y-m-d H:i:s'));

        $opportunities = json_decode(CapsuleBase::getV2('opportunities', array(
            'since' => CapsuleBase::formatDate($lastModified),
            'embed' => 'fields,milestone'
        )));

        $sync = array();
        $customFields = array();

        /*
         * API V2 does not return @size so needs to get the size of the array
         */
        if ( isset( $opportunities->opportunities ) ){

            foreach( $opportunities->opportunities as $o ) {

                $tempOpportunity = array(
                    'id'                => $o->id,
                    'name'              => $o->name,
                    'description'       => $o->description,
                    'partyId'           => $o->party->id,
                    'currency'          => $o->value->currency,
                    'value'             => $o->value->amount . '.00',
                    'expectedCloseDate' => date('Y-m-d H:i:s',strtotime($o->expectedCloseOn)),
                    'actualCloseDate'   => date('Y-m-d H:i:s',strtotime($o->closedOn)),
                    'milestoneId'       => $o->milestone->id,
                    'milestone'         => $o->milestone->name,
                    'probability'       => $o->probability,
                    'owner'             => $o->owner->username,
                    'createdOn'         => date('Y-m-d H:i:s',strtotime($o->createdAt)),
                    'updatedOn'         => date('Y-m-d H:i:s',strtotime($o->updatedAt))
                );

                $capsuleOpportunity = CapsuleOpportunity::findById( $o->id );

                // Update opportunity if it exists
                 if( $capsuleOpportunity ) {
                    CapsuleBase::syncLog('info', 'Existing Opportunity '. $o->id . '. Updating instead.');
                    $capsuleOpportunity->save( $tempOpportunity );
                    CapsuleBase::syncLog('info','Updated Opportunity '. $o->id );

                // Create a new entry
                } else {
                    CapsuleBase::syncLog('info','Importing Opportunity '. $o->id );
                    $capsuleOpportunity = CapsuleOpportunity::create();
                    $capsuleOpportunity->save( $tempOpportunity );
                    CapsuleBase::syncLog('info', 'Imported Opportunity '. $o->id ) ;

                }

                $sync[] = $capsuleOpportunity->id;

                /**
                 * Update to get the custom fields
                 */
                if( isset( $o->fields ) ) {
                    $opportunity = CapsuleOpportunity::findById( $o->id );

                    if( $opportunity ){
                        CapsuleBase::syncLog('info', 'Saving custom fields for Opportunity '. $o->id );

                        foreach( $o->fields as $field ){
                            array_push( $customFields, array(
                                "id" => $field->id,
                                "label" => $field->definition->name,
                                "text"  => $field->value
                            ));
                        }
                        // echo '<pre>';
                        // print_r($customFields);
                        // echo '</pre>';
                        $opportunity->customFields = serialize( $customFields );
                        // echo $opportunity->customFields;
                        // echo '<pre>';
                        // print_r( unserialize($opportunity->customFields) );
                        // echo '</pre>';
                        $opportunity->save();
                     }
                }
            }

            if( count( $sync) > 0){
                return CapsuleOpportunity::find('all', array(
                        'conditions'=>array(
                            'id'=> $sync
                        )
                ));
            }

        }

    }




}

?>
