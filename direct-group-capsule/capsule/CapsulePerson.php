<?php
namespace app\models\capsule;

use lithium\analysis\Logger;
use app\models\capsule\CapsuleContact;

class CapsulePerson extends CapsuleBase {
    public $_meta = array('source'=>'capsule_person');

    public $hasMany  = array(
        'History'=>array(
            'to'=>'app\models\capsule\CapsuleHistory',
            'key'=>'partyId'
        ),
        'Contact'=>array(
            'to'=>'app\models\capsule\CapsuleContact',
            'key'=>array(
                'id'=>'partyId'
            )

        )
    );

    public function name($entity)
    {
        return $entity->firstName.' '.$entity->lastName;
    }

    public function title($entity)
    {
        return $entity->name();
    }

    public function getContact($entity, $contactType, $type=null) {
        $conditions = array(
            'partyId'=>$entity->id,
            'contact_type'=>$contactType
        );
        if($type) {
            $conditions['type'] = $type;
        }
        $contact = CapsuleContact::find('first', array(
                'conditions'=>$conditions
            ));

        Logger::write('info', json_encode($contact));

        return $contact;
    }

    public function getCustomFields($entity){
        $customFields = array();
        if(!empty($entity->customFields)) {
            $customFields = unserialize($entity->customFields);
        }
        return $customFields;

    }

    public function getFieldValue($entity, $field) {
        $customFields = $this->getCustomFields($entity);
        if(isset($customFields['id'])) {
            $customFields = array($customFields); // Normalize
        }
        foreach($customFields as $cF) {
            if($cF['label'] == $field) {
                if(isset($cF['boolean'])){ return $cF['boolean']; }
                if(isset($cF['text'])){ return $cF['text']; }
            }
        }
        return false;
    }

    public static function sync(\DateTime $lastModified)
    {
        CapsuleBase::syncLog('info', 'Starting import process for people, last modified '.$lastModified->format('Y-m-d H:i:s'));

        $people = json_decode( CapsuleBase::getV2('parties', array(
            'since' => CapsuleBase::formatDate($lastModified),
            'embed' => 'fields'
        )));

        $sync = array();
        $customFields = array();

        //echo '<pre>'; print_r( $people ); echo '</pre>';

        if( isset( $people->parties ) ) {

            foreach( $people->parties as $person ) {
                if( $person->type == 'person' ) {
                    $tempPerson = array(
                        'id'                => $person->id,
                        'title'             => $person->title,
                        'firstName'         => $person->firstName,
                        'lastName'          => $person->lastName,
                        'about'             => $person->about,
                        'jobTitle'          => $person->jobTitle,
                        'pictureURL'        => $person->pictureURL,
                        'createdOn'         => date('Y-m-d H:i:s',strtotime($person->createdAt)),
                        'updatedOn'         => date('Y-m-d H:i:s',strtotime($person->updatedAt))
                    );

                    if( isset( $person->organisation ) ){
                        $tempPerson['organisationId'] = $person->organisation->id;
                        $tempPerson['organisationName'] = $person->organisation->name;
                    }

                    $capsulePerson = CapsulePerson::findById( $person->id );
                    if( $capsulePerson ) {
                        CapsuleBase::syncLog('info','Existing Person '.  $person->id .'. Updating instead.');
                        $capsulePerson->save($tempPerson);
                        CapsuleBase::syncLog('info','Updated Person '. $person->id );
                    }else{
                        CapsuleBase::syncLog('info','Importing Person '. $person->id );
                        $capsulePerson = CapsulePerson::create();
                        $capsulePerson->save($tempPerson);
                        CapsuleBase::syncLog('info', 'Imported Person '. $person->id );
                    }

                    $sync[] = $capsulePerson->id;

                    self::saveContacts( $person );
                    self::saveCustomFields( $person );
                    self::saveHistory( $person );
                }

            }

            if( count($sync) > 0){
                return CapsulePerson::find('all', array(
                        'conditions'=>array(
                            'CapsulePerson.id'=>$sync
                        ),
                        'with'=>array('Contact')
                    ));
            }
        }

    }

    /**
     * History is now referred to as Entries in V2
     * @params  id      the organisation id
     * @return void
     */
    protected static function saveHistory( $person )
    {
        $uri = 'parties/' . $person->id . '/entries';
        $histories = json_decode( CapsuleBase::getV2($uri, array(
            'embed' => 'party'
        )));

        if( isset( $histories->entries ) ) {

            foreach( $histories->entries  as $history ) {

                $tempHistory = array(
                    'id'            => $history->id,
                    'type'          => $history->type,
                    'creator'       => $history->creator->username,
                    'creatorName'   => $history->creator->name
                );
                $capsuleHistory = CapsuleHistory::findById($history->id);

                if( isset($history->entryAt) ) {
                    $tempHistory['entryDate'] = date('Y-m-d H:i:s',strtotime($history->entryAt ));
                }

                if( isset($history->attachments) ) {
                    $tempHistory['attachments'] = serialize( $history->attachments );
                }
                if( isset($history->participants) ) {
                    $tempHistory['participants'] = serialize( $history->participants );
                }
                if( isset($history->subject) ) {
                    $tempHistory['subject'] = $history->subject;
                }
                if( isset($history->content) ) {
                    $tempHistory['content'] = $history->content;
                }
                if( isset($history->party ) ) {
                    $tempHistory['partyId'] = $history->party->id;
                    $tempHistory['partyName'] = $history->party->name;
                }

                if($capsuleHistory){
                   CapsuleBase::syncLog('info', 'Updating history '. $history->id);
                   $capsuleHistory->save( $tempHistory );
                }else{
                   CapsuleBase::syncLog('info', 'Creating history '. $history->id);
                   $capsuleHistory = CapsuleHistory::create();
                }
            }
        }

    }

    /**
     * Saving contacts
     * @return
     */
    protected static function saveContacts( $person )
    {
        if(isset( $person->phoneNumbers ) && count( $person->phoneNumbers ) > 0) {
            foreach( $person->phoneNumbers as $phoneNum ) {
                $capsulePhoneNumbers = CapsuleContact::findById( $phoneNum->id );
                $tempContact = array(
                   'id'                => $phoneNum->id,
                   'partyId'           => $person->id,
                   'contact_type'      => 'phone',
                   'type'              => $phoneNum->type,
                   'phoneNumber'       => $phoneNum->number
                );

                if( $capsulePhoneNumbers ) {
                    CapsuleBase::syncLog('info','Existing contact '. $phoneNum->id .'. Updating instead.');
                    $capsulePhoneNumbers->save( $tempContact );
                } else {
                     $capsulePhoneNumbers = CapsuleContact::create();
                     $capsulePhoneNumbers->save( $tempContact );

                }

            }

        } //end phone numbers

        if(isset( $person->emailAddresses ) && count( $person->emailAddresses ) > 0) {
            foreach( $person->emailAddresses as $emailAdd ) {
                $capsuleEmailAddresses = CapsuleContact::findById( $emailAdd->id );
                $tempContact = array(
                    'id'                => $emailAdd->id,
                    'partyId'           => $person->id,
                    'contact_type'      => 'email',
                    'type'              => $emailAdd->type,
                    'emailAddress'      => $emailAdd->address
                );
                if( $capsuleEmailAddresses ) {
                    $capsuleEmailAddresses->save( $tempContact );
                }else {
                    $capsuleEmailAddresses = CapsuleContact::create();
                    $capsuleEmailAddresses->save( $tempContact );
                }
            }
        } //end email addresses

        if(isset( $person->websites ) && count( $person->websites ) > 0 ) {
            foreach( $person->websites as $website ) {
                $tempContact = array(
                    'id'                => $website->id,
                    'partyId'           => $person->id,
                    'contact_type'      => null,
                    'type'              => $website->type,
                    'webAddress'        => $website->address,
                    'webService'        => $website->service,
                    'url'               => $website->url
                );
                $capsuleWebsites = CapsuleContact::findById( $website->id );
                if( $capsuleWebsites ) {
                    $capsuleWebsites->save( $tempContact );
                }else {
                    $capsuleWebsites = CapsuleContact::create();
                    $capsuleWebsites->save( $tempContact );
                }
            }
        } //end websites
    }

    /**
     * Saving custom fields
     * @return
     */
    protected static function saveCustomFields( $person )
    {
        $customFields = array();
        if( isset( $person->fields ) &&  count( $person->fields ) > 0) {
            $personCap = CapsulePerson::findById( $person->id );

            if( $personCap ){

                CapsuleBase::syncLog('info', 'Saving custom fields for Opportunity '. $person->id );

                foreach( $person->fields as $field ){
                    array_push( $customFields, array(
                        'id'    => $field->id,
                        'label' => $field->definition->name,
                        'text'  => $field->value
                    ));
                }

                $personCap->customFields = serialize( $customFields );
                $personCap->save();
             }
        }
    }
}

?>
